use std::process::Command;
use std::str;

pub fn compress_gz(dest: &str, src: &str) {
    let compress = Command::new("tar")
        .arg("-czf")
        .arg(&dest)
        .arg(&src)
        .output()
        .expect("Failed to compress file!");
    compress.stdout;
}

pub fn compress_bz2(dest: &str, src: &str) {
    let compress = Command::new("tar")
        .arg("-cjf")
        .arg(&dest)
        .arg(&src)
        .output()
        .expect("Failed to compress file!");
    compress.stdout;
}

pub fn compress_xz(dest: &str, src: &str) {
    let compress = Command::new("tar")
        .arg("-cJf")
        .arg(&dest)
        .arg(&src)
        .output()
        .expect("Failed to compress file!");
    compress.stdout;
}
