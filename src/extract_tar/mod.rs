use std::process::Command;
use std::str;

pub fn extract_gz(file: &str) {
    let extract = Command::new("tar")
        .arg("-xzf")
        .arg(&file)
        .output()
        .expect("Failed to extact file!");
    extract.stdout;
}

pub fn extract_xz(file: &str) {
    let extract = Command::new("tar")
        .arg("-xf")
        .arg(&file)
        .output()
        .expect("Failed to extract file!");
    extract.stdout;
}

pub fn extract_bz2(file: &str) {
    let extract = Command::new("tar")
        .arg("-xjf")
        .arg(&file)
        .output()
        .expect("Failed to extract file!");
    extract.stdout;
}
